package service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.apache.tinkerpop.gremlin.process.traversal.Order;
import org.apache.tinkerpop.gremlin.process.traversal.Path;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Direction;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.io.graphml.GraphMLReader;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import org.javatuples.Pair;
import org.springframework.stereotype.Service;

import service_interface.SmellsAnalyze;

import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.*;

@Service
public class SmellCatcher implements SmellsAnalyze{
	private Graph graph;
	private String path;
	private int pointer;
	private int sum = 0;
	File file;
	

	private double median;
	public SmellCatcher() {
		this.path = "";
		graph = TinkerGraph.open();
		InputStream fip = null;
		String workingDirectory = System.getProperty("user.dir");
        
        
		
		try {
			path = "graph.graphml";
			file = new File(path);
			if(file.exists()) {
				fip = new FileInputStream(file);
				GraphMLReader.build().create().readGraph(fip, graph);
			}
		} catch (IOException e) {
			System.out.println("graphml non trovato");
			e.printStackTrace();
		} finally {
			try {
				if (fip != null) {
					fip.close();
				}
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
	}
	
	

	@Override
	public void lackAPIGateway() {
		System.out.println();
		System.out.println();
		String name;
		boolean check = false;
		List<Vertex> v = graph.traversal().V().hasLabel("service_interface").has("name").toList();
		for(int i = 0; i < v.size(); i++) {
			if(v.get(i).value("name").toString().toLowerCase().contains("gateway".toLowerCase()))
				check = true;		
		}
		if(check)
			System.out.println("Presence of API Gateway confirmed");
		else
			System.out.println("Mismatch in the search of API Gateway, multiple occurrences");
	}
	@Override
	public void megaService() {
		System.out.println();
		System.out.println();
		List<Edge> eEnd = graph.traversal().V().hasLabel("service_interface").inE("call").order().by("weight", Order.desc).toList();
		List <Integer> weight = new ArrayList<Integer>();
		int client_index = -1;
		for(int i = 0; i < eEnd.size(); i++) {
			weight.add(eEnd.get(i).value("weight"));
			sum += weight.get(i).intValue();
			System.out.println("w:"+weight.get(i)+ "   " +eEnd.get(i).outVertex().value("name")+" ---->: v:"+ eEnd.get(i).inVertex().value("name"));

			if(eEnd.get(i).outVertex().value("name").toString().compareTo("client") == 0 && eEnd.get(i).inVertex().value("name").toString().toLowerCase().contains("gateway".toLowerCase())) {
				
				client_index = i;
			}
		}
		Collections.sort(weight);
		if (weight.size() % 2 == 0)
		    median = ((double)weight.get(weight.size()/2) + (double)weight.get((weight.size()/2) - 1));
		else
		    median = (double) weight.get(weight.size()/2);
		System.out.println("median: "+median+" sum:"+sum +"  sum/weight.size():"+sum/weight.size());
		
		for(int i = 0; i < weight.size(); i++) {
			if(median == weight.get(i)) {
				pointer = i;
			}
		}
		int totalsum = 0;
		for(int i = 0; i <= pointer; i++) {
			totalsum += weight.get(i);
		}
		int megaPointer = -1;
		
		for(int i = pointer; i < weight.size(); i++) {
			if(totalsum < weight.get(i))
				megaPointer = i;
			if(client_index > 0) {
				megaPointer = client_index;
			}
		}
		
		System.out.println("MegaService analysis:");
		if(megaPointer == -1)
			System.out.print("No MegaService discovered");
		else {
			System.out.println("  Currently MegaServices:");
			for(int i = megaPointer-1; i >= 0; i--) {
				if(eEnd.get(i).inVertex().value("name").toString().compareTo(eEnd.get(i).outVertex().value("name").toString())==0) {
					System.out.println("  Self-loop communication:");
					System.out.println("["+i+"]"+"    start: "+eEnd.get(i).outVertex().value("name")+" --"+ eEnd.get(i).value("weight")+"--> end: " + eEnd.get(i).inVertex().value("name"));
				}else
					System.out.println("["+i+"]"+"    start: "+eEnd.get(i).outVertex().value("name")+" --"+ eEnd.get(i).value("weight")+"--> end: " + eEnd.get(i).inVertex().value("name"));
			}
			System.out.println("  Currently not MegaServices:");
			for(int i = megaPointer; i < weight.size(); i++) {
				System.out.println("["+i+"]"+"    start: "+eEnd.get(i).outVertex().value("name")+" --"+ eEnd.get(i).value("weight")+"--> end: " + eEnd.get(i).inVertex().value("name"));
			}
		}
			
	}


	 @Override
	public void cyclicDependency() {
		 System.out.println();
		 System.out.println();
		 List<Edge> eEnd = graph.traversal().V().hasLabel("service_interface").inE("call").toList();
		 List<Vertex> v = graph.traversal().V().hasLabel("service_interface").has("name").toList();

		 /*for (int x = 0; x < eEnd.size(); x++) {
			 System.out.print(eEnd.get(x).toString());
			 System.out.print(eEnd.get(x).outVertex().toString() + ":" + eEnd.get(x).outVertex().value("name") + "---->" + eEnd.get(x).inVertex().toString() + ":" + eEnd.get(x).inVertex().value("name"));
			 System.out.println();
		 }*/
		 printList(v);
		 List<String> serviceNames = new ArrayList<>();
		 //Lista dei nomi dei servizi che verrà svuotata una volta riempita la lista di adiacenza
		 for (int i = 0; i < v.size(); i++) {
			 if(!serviceNames.contains(v.get(i).value("name").toString()))
				 serviceNames.add(v.get(i).value("name").toString());
		 }
		 List<String> tmpNames = new ArrayList<>();

		 ArrayList<ArrayList<Integer>> g = new ArrayList<>(v.size());
		 ArrayList<Integer> neighbors = null;
		 for (int i = 0; i < eEnd.size(); i++) {
			 //creo una nuova lista di neighbors
			 neighbors = new ArrayList<Integer>();
			 //se la lista dei serviceName contiene già il nome presente
			 if (!tmpNames.contains(eEnd.get(i).outVertex().value("name"))) {
				 tmpNames.add(eEnd.get(i).outVertex().value("name"));
				 for (int j = 0; j < eEnd.size(); j++) {
					 if (eEnd.get(i).outVertex().value("name").toString().compareTo(eEnd.get(j).outVertex().value("name").toString()) == 0) {
						 //se nei neighbors non è contenuto il serviceName
						 if(!neighbors.contains(serviceNames.indexOf(eEnd.get(j).inVertex().value("name").toString())))
							 //aggiungi il serviceName alla lista dei neighbors
						 	neighbors.add(serviceNames.indexOf(eEnd.get(j).inVertex().value("name").toString()));
					 }
				 }
			 }
			 g.add(neighbors);
			 for (int x = 0; x < neighbors.size(); x++) {
				 System.out.println("["+ serviceNames.indexOf(eEnd.get(i).outVertex().value("name").toString()) +"]"+"servicename:"+eEnd.get(i).outVertex().value("name").toString()+"---->neighbor_name:"+serviceNames.get(neighbors.get(x))+"["+ serviceNames.indexOf(serviceNames.get(neighbors.get(x))) +"]");
			 }


		 }

		 if(isCyclic(v.size(), g))
			 System.out.println("Cycle detected");
		 else
			 System.out.println("No cycles detected");
	 }

	public boolean isCyclic(int N, ArrayList<ArrayList<Integer>> adj) {
		int vis[] = new int[N];
		int dfsVis[] = new int[N];

		for(int i = 0;i<N;i++) {
			if(vis[i] == 0) {
				if(checkCycle(i, adj, vis, dfsVis) == true) return true;
			}
		}
		return false;
	}
	@Override
	public boolean checkCycle(int node,  ArrayList<ArrayList<Integer>> adj, int vis[], int dfsVis[]) {
		vis[node] = 1;
		dfsVis[node] = 1;

		for(Integer neighbor: adj.get(node)) {
			if(vis[neighbor] == 0) {
				if(checkCycle(neighbor, adj, vis, dfsVis) == true) {
					return true;
				}
			} else if(dfsVis[neighbor] == 1) {
				return true;
			}
		}
		dfsVis[node] = 0;
		return false;
	}

	@Override
	public void sharedPersistence() {
		System.out.println();
		System.out.println();
		List<Edge> eEnd = graph.traversal().V().hasLabel("service_interface").inE("call").toList();
		List<String> dbs = new ArrayList<>();
		for(int i = 0; i < eEnd.size(); i++) {
			if(eEnd.get(i).property("isDB").toString().toLowerCase().contains("true".toLowerCase())) {
				String db = eEnd.get(i).inVertex().property("name").toString();
				if(!dbs.contains(db)){
					dbs.add(db);
				}else{
					System.out.println("Shared Dependence on db:"+ db +". From the following services:");

					for(int j = 0; j < eEnd.size(); j++) {
						if (eEnd.get(j).property("isDB").toString().toLowerCase().contains("true".toLowerCase())) {
							if (eEnd.get(j).inVertex().property("name").toString().compareTo(db) == 0) {
								System.out.println(eEnd.get(j).outVertex().property("name").toString());
							}
						}
					}
				}
			}
		}

	}




	public void hardcoded() {}
	
	private void printList(List<Vertex> listToPrint) {

		for(int i = 0; i < listToPrint.size(); i++)
			System.out.println("["+i+"]"+listToPrint.get(i).value("name").toString());

	}

	public Graph getGraph() {
		return graph;
	}


	public void setGraph(Graph graph) {
		this.graph = graph;
	}


	public int getPointer() {
		return pointer;
	}


	public void setPointer(int pointer) {
		this.pointer = pointer;
	}


	public int getSum() {
		return sum;
	}


	public void setSum(int sum) {
		this.sum = sum;
	}


	public double getMedian() {
		return median;
	}


	public void setMedian(double median) {
		this.median = median;
	}


	public String getPath() {
		return path;
	}
}
