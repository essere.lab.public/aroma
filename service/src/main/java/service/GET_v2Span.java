package service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.io.graphml.GraphMLWriter;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import ch.qos.logback.core.joran.conditional.ElseAction;
import model.EndPoint;
import model.Span;
import service_interface.GraphBuilder;

@Service
public class GET_v2Span implements GraphBuilder{
	private List<Span> spans;
	private List<Span> parent;
	private List<String> singleOccurrence;
	private static final String filename = "spans.json";
	private Graph g = null;
	private final String path = "graph.graphml";
	
	/**
	 *  Constructor
	 * @return 
	 */
	public GET_v2Span() {
		this.singleOccurrence = new ArrayList<String>();
		this.spans = new ArrayList<Span>();
		this.parent = new ArrayList<Span>();
		this.g = TinkerGraph.open();
	}
	
	@Override
	public boolean createV2() {
		boolean isPopulated = populateList();
		if(isPopulated == false) {
			System.console().writer().println("File json containing spans is empty");
			return false;
		}
		generateCallGraph();
		return true;
	}
	
	private boolean populateList() {
		//Span class parameters
		String id = "";
		String parentId = "";
		int duration = 1;
		String serviceNameL = "";
		String serviceNameR = "";
		Date date;
		String timeUTC = "";
		//EndPoint class parameters
		int localPort = -1;
		int remotePort = -1;
		String localIpv4 = "";
		String remoteIpv4 = "";
		Boolean hasDB = false;
		String dbName = "";

		Gson gson = new Gson();
		Object obj = null;
		JsonElement je;
		try {
			obj = gson.fromJson(new FileReader(filename), Object.class);
			if(obj == null)
				return false;
			je = gson.toJsonTree(obj);
			
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
			return false;
		} catch (JsonIOException e) {
			e.printStackTrace();
			return false;
		} catch (FileNotFoundException e) {
			e.printStackTrace(); 
			return false;
		} catch (IllegalStateException e){
			e.printStackTrace();
			return false;
		}
		JsonArray ja = je.getAsJsonArray();

		
		// populate span list from json
		for(int i = 0; i < ja.size();i++) {
			JsonArray ja_a = ja.get(i).getAsJsonArray();
			JsonObject ja_a_Obj = ja_a.get(0).getAsJsonObject();
			for(int j = 0; j < ja_a.size(); j++) {
				ja_a_Obj =  ja_a.get(j).getAsJsonObject();
				// String id cannot be null
				id = ja_a_Obj.get("id").toString();
				// String parentId can be null
				parentId = "";
				if(ja_a_Obj.has("parentId")) 
					parentId = ja_a_Obj.get("parentId").getAsString();
				// int duration min value = 1
				duration = ja_a_Obj.get("duration").getAsInt();
				// String timeUTC can be null
				timeUTC = "";
				if(ja_a_Obj.has("timestamp")) {
					long epochTime = TimeUnit.SECONDS.convert(ja_a_Obj.get("timestamp").getAsLong(), TimeUnit.MILLISECONDS);
					date = new Date(epochTime);
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm z");
					formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
					timeUTC = formatter.format(date).toString();
				}
				serviceNameL = "";
				serviceNameR = "";
				//if localEndpoint and remoteEndpoint exists
				if(ja_a_Obj.has("localEndpoint") && ja_a_Obj.has("remoteEndpoint")) {

					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("serviceName"))
						serviceNameL = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("serviceName").getAsString();
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("port"))
						localPort = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("port").getAsInt();
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("ipv4"))
						localIpv4 = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("ipv4").getAsString();
					
					if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("serviceName"))
						serviceNameR = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("serviceName").getAsString();
					if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("port"))
						remotePort = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("port").getAsInt();
					if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("ipv4"))
						remoteIpv4 = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("ipv4").getAsString();

					//if tag exists and is database
					if(ja_a_Obj.has("tags") && ja_a_Obj.get("tags").getAsJsonObject().has("db.system")) {
						hasDB = true;
						dbName = ja_a_Obj.get("tags").getAsJsonObject().get("db.system").getAsString();
					}else if(ja_a_Obj.has("tags") && ja_a_Obj.get("tags").getAsJsonObject().has("db")) {
						hasDB = true;
						dbName = ja_a_Obj.get("tags").getAsJsonObject().get("db").getAsString();
					}else hasDB = false;
					if(!hasDB) {
						spans.add(new Span(
								id.replaceAll("^\"|\"$", ""),
								parentId.replaceAll("^\"|\"$", ""),
								duration,
								timeUTC.replaceAll("^\"|\"$", ""),
								serviceNameL.replaceAll("^\"|\"$", ""),
								new EndPoint(id.replaceAll("^\"|\"$", ""), 
											"local".replaceAll("^\"|\"$", ""),
											localPort, 
											localIpv4.replaceAll("^\"|\"$", ""), 
											serviceNameL.replaceAll("^\"|\"$", "")),
								new EndPoint("".replaceAll("^\"|\"$", ""), 
											"remote".replaceAll("^\"|\"$", ""), 
											remotePort, 
											remoteIpv4.replaceAll("^\"|\"$", ""), 
											serviceNameR.replaceAll("^\"|\"$", "")),
											dbName,
											hasDB
								)); 
								hasDB=false; 
								dbName = "";
					}else{
						//arco entrante nel servizio che ha il db
						spans.add(new Span(
								id.replaceAll("^\"|\"$", ""),
								parentId.replaceAll("^\"|\"$", ""),
								duration,
								timeUTC.replaceAll("^\"|\"$", ""),
								serviceNameL.replaceAll("^\"|\"$", ""),
								new EndPoint(id.replaceAll("^\"|\"$", ""),
										"local".replaceAll("^\"|\"$", ""),
										localPort,
										localIpv4.replaceAll("^\"|\"$", ""),
										serviceNameL.replaceAll("^\"|\"$", "")),
								new EndPoint("".replaceAll("^\"|\"$", ""),
										"remote".replaceAll("^\"|\"$", ""),
										remotePort,
										remoteIpv4.replaceAll("^\"|\"$", ""),
										"".replaceAll("^\"|\"$", "")),
								dbName,
								false
						));
						//arco uscente dal servizio che ha db
						spans.add(new Span(
								id.replaceAll("^\"|\"$", ""),
								parentId.replaceAll("^\"|\"$", ""),
								duration,
								timeUTC.replaceAll("^\"|\"$", ""),
								dbName.replaceAll("^\"|\"$", ""),
								new EndPoint(id.replaceAll("^\"|\"$", ""),
										"local".replaceAll("^\"|\"$", ""),
										localPort,
										localIpv4.replaceAll("^\"|\"$", ""),
										dbName.replaceAll("^\"|\"$", "")),
								new EndPoint("".replaceAll("^\"|\"$", ""),
										"remote".replaceAll("^\"|\"$", ""),
										remotePort,
										remoteIpv4.replaceAll("^\"|\"$", ""),
										serviceNameL.replaceAll("^\"|\"$", "")),
								dbName,
								hasDB
						));

						hasDB=false; dbName = "";
					}
				}
				//if ONLY localEndpoint exists
				else if(ja_a_Obj.has("localEndpoint")){
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("serviceName"))
						serviceNameL = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("serviceName").getAsString();
					
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("port"))
						localPort = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("port").getAsInt();
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("ipv4"))
						localIpv4 = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("ipv4").getAsString();

					remotePort = -1;
					remoteIpv4 = "";
					//if tag exists and is database
					if(ja_a_Obj.has("tags") && ja_a_Obj.get("tags").getAsJsonObject().has("db.system")) {
						hasDB = true;
						dbName = ja_a_Obj.get("tags").getAsJsonObject().get("db.system").getAsString();
					}else if(ja_a_Obj.has("tags") && ja_a_Obj.get("tags").getAsJsonObject().has("db")) {
						hasDB = true;
						dbName = ja_a_Obj.get("tags").getAsJsonObject().get("db").getAsString();
					}else hasDB=false;
					if(!hasDB) {
						spans.add(new Span(
								id.replaceAll("^\"|\"$", ""),
								parentId.replaceAll("^\"|\"$", ""),
								duration,
								timeUTC.replaceAll("^\"|\"$", ""),
								serviceNameL.replaceAll("^\"|\"$", ""),
								new EndPoint(id.replaceAll("^\"|\"$", ""),
											"local".replaceAll("^\"|\"$", ""),
											localPort,
											localIpv4.replaceAll("^\"|\"$", ""),
											serviceNameL.replaceAll("^\"|\"$", "")),
								new EndPoint("".replaceAll("^\"|\"$", ""),
											"remote".replaceAll("^\"|\"$", ""),
											remotePort,
											remoteIpv4.replaceAll("^\"|\"$", ""),
											serviceNameR.replaceAll("^\"|\"$", "")),
											dbName,
											hasDB
						));hasDB=false; dbName = "";
					}else{
						spans.add(new Span(
								id.replaceAll("^\"|\"$", ""),
								parentId.replaceAll("^\"|\"$", ""),
								duration,
								timeUTC.replaceAll("^\"|\"$", ""),
								serviceNameL.replaceAll("^\"|\"$", ""),
								new EndPoint(id.replaceAll("^\"|\"$", ""),
										"local".replaceAll("^\"|\"$", ""),
										localPort,
										localIpv4.replaceAll("^\"|\"$", ""),
										serviceNameL.replaceAll("^\"|\"$", "")),
								new EndPoint("".replaceAll("^\"|\"$", ""),
										"remote".replaceAll("^\"|\"$", ""),
										remotePort,
										remoteIpv4.replaceAll("^\"|\"$", ""),
										"".replaceAll("^\"|\"$", "")),
								dbName,
								hasDB
						));
						spans.add(new Span(
								id.replaceAll("^\"|\"$", ""),
								parentId.replaceAll("^\"|\"$", ""),
								duration,
								timeUTC.replaceAll("^\"|\"$", ""),
								dbName.replaceAll("^\"|\"$", ""),
								new EndPoint(id.replaceAll("^\"|\"$", ""),
										"local".replaceAll("^\"|\"$", ""),
										localPort,
										localIpv4.replaceAll("^\"|\"$", ""),
										dbName.replaceAll("^\"|\"$", "")),
								new EndPoint("".replaceAll("^\"|\"$", ""),
										"remote".replaceAll("^\"|\"$", ""),
										remotePort,
										remoteIpv4.replaceAll("^\"|\"$", ""),
										serviceNameL.replaceAll("^\"|\"$", "")),
								dbName,
								hasDB
						));
						hasDB=false; dbName = "";
					}
				}
				//if ONLY remoteEndpoint exists
				else if(ja_a_Obj.has("remoteEndpoint")){
						if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("serviceName"))
							serviceNameR = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("serviceName").getAsString();
						if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("port"))
							remotePort = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("port").getAsInt();
						if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("ipv4"))
							remoteIpv4 = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("ipv4").getAsString();
				}
			}	
		
		}
		printList(spans);
		String parentIdRelated = "";
		//
		//
		//
		//
		//populate i remoteEndPoint di ogni span con il parent associato
		for(int k = 0; k < spans.size(); k++) {
			//se esiste un parentId allora devo aggiungere il nome del servizio ai servizi associati nel remote
			if(spans.get(k).getRemoteEndpoint().getServiceName().compareTo("") == 0){
				if(spans.get(k).getParentId() != "") {
					//ottenuto il parent id da usare per la ricerca dei remote da fillare
					parentIdRelated = spans.get(k).getParentId();
				}/*else {
					spans.get(k).getRemoteEndpoint().setServiceName("client");
					}*/
				for(int r = 0; r < spans.size(); r++) {
						if (spans.get(r).getId().compareTo(parentIdRelated) == 0) {
							spans.get(k).getRemoteEndpoint().setServiceName(spans.get(r).getServiceName());
						}
				}
				parentIdRelated = "";
			}
		}
		printList(spans);
		return true;
	}
	
	
	/**
	 * @param listToPrint
	 */
	private void printList(List<Span> listToPrint) {
		System.out.println("startPrint");
		for(int i = 0; i < listToPrint.size(); i++)
			System.out.println("["+i+"]"+listToPrint.get(i).toStringV2());
		System.out.println("endPrint");
	}

	/**
	 * @param listToPrint
	 */
	private void printListDebug(List<String> listToPrint) {
		System.out.println("startPrint");
		for(int i = 0; i < listToPrint.size(); i++)
			System.out.println("["+i+"]"+listToPrint.get(i).toString());
		System.out.println("endPrint");
	}
	public void printStructure() {
		printList(parent);
	}
	/**
	 * @param list
	 * @param name
	 * @return
	 */
	public boolean pContainsName(final List<Span> list, final String name){
	    //return list.stream().filter(o -> o.getServiceName().equals(name)).findFirst().isPresent();
		for(int i = 0; i < list.size(); i++) {
			if(list.get(i).getServiceName().compareTo(name) == 0) {
				return true;
			}
				
		}
		return false;
	}

	private int getIndexByName(final List<Span> list, final String name) {
		for(int i = 0; i < list.size(); i ++) {
			if (list.get(i).getServiceName().compareTo(name) == 0)
				return i;
		}
		return -1;
	}

	private void generateCallGraph() {
		/*for(int i = 0; i < 100; i++){
			System.out.println("["+i+"]"+spans.get(i).toStringV2());
		}*/
		// populate parent list
		for(int i = 0; i < spans.size(); i++) {
			if(!pContainsName(parent, spans.get(i).getRemoteEndpoint().getServiceName())){
				int tmp = getIndexByName(spans, spans.get(i).getRemoteEndpoint().getServiceName());
				if(tmp >= 0) {
					parent.add(spans.get(tmp));
				}
				else if(!pContainsName(parent, spans.get(i).getServiceName())){
						if(tmp < 0 && spans.get(i).getHasDB()){
							parent.add(spans.get(i));
						}
				}

				/*else
				parent.add(new Span(
							"client".replaceAll("^\"|\"$", ""),
							"client".replaceAll("^\"|\"$", ""),
							0,
							"".replaceAll("^\"|\"$", ""),
							"client".replaceAll("^\"|\"$", ""),
							new EndPoint("client".replaceAll("^\"|\"$", ""),"local".replaceAll("^\"|\"$", ""),0,"client".replaceAll("^\"|\"$", ""),"".replaceAll("^\"|\"$", "")),
							new EndPoint("".replaceAll("^\"|\"$", ""),"remote".replaceAll("^\"|\"$", ""),0,"client".replaceAll("^\"|\"$", ""),"".replaceAll("^\"|\"$", ""))
							dbName,
							hasDB));*/
			}
		}
		//System.out.println("Print parent list:");
		printList(parent);
		for(int i = 0; i < parent.size(); i++){
			System.out.println("["+i+"]"+parent.get(i).getServiceName());
		}
		String nameTFinRemote = "";
		
		// populate know list for each parent
		for(int i = 0; i < parent.size(); i++) {
			ArrayList<Span> tSpans = new ArrayList<>();

			//get the parent serviceName
			nameTFinRemote = parent.get(i).getServiceName();
			System.out.println("nameTFinRemote:"+nameTFinRemote);
			//looking for dependencies
			for(int j = 0; j < spans.size(); j++) {
				//Se il remoteEndpoint è il parent
				if(spans.get(j).getRemoteEndpoint().getServiceName().compareTo(nameTFinRemote) == 0) {
					//e se il servizio non è già presente nella lista dei figli
					if(!pContainsName(parent.get(i).getKnow(), spans.get(j).getServiceName())){
						//e se il parent non è uguale al servizio
						//if(nameTFinRemote.compareTo(spans.get(j).getServiceName())!=0)
							//parent.get(i).getKnow().add(spans.get(j));
							tSpans.add(spans.get(j));
					}
				}			
			}
			
			//rimuovo duplicati e self-loop 
			List<Span> uniqueSons = tSpans
									.stream()
									.distinct()
									.collect(Collectors.toList());
			
			parent.get(i).getKnow().addAll(uniqueSons);								
		}


		printList(parent);
		List<Vertex> vertexes = new ArrayList <Vertex>();
		List<String> uniqueSon = new ArrayList <String> ();
		List<String> removing = new ArrayList <String> ();
		// look for the serviceName to which to link the list in position i of parent
		// do not create duplicates of the objects, I create a list of vertex addresses associated with the service_interface
		for(int i = 0; i < parent.size(); i++) {
			for(int j = 0; j < parent.get(i).getKnow().size(); j++) {
				// serviceName of parent (A) not in uniqueSon and not in vertexes
				if(!includeStringInList(uniqueSon, parent.get(i).getServiceName())
						&& !includeStringInVertexes(vertexes, parent.get(i).getServiceName())) {
					uniqueSon.add(parent.get(i).getServiceName());
					// add to vertexes a vertex object (not duplicated) with the correct properties
					vertexes.add(addVertex(g, parent.get(i).getServiceName()));
				}
				// serviceName remoteEndpoint (B) in know not present in the uniqueParent list
				if(!includeStringInList(uniqueSon, parent.get(i).getKnow().get(j).getServiceName())) {
					removing.add(parent.get(i).getKnow().get(j).getServiceName());
					
					uniqueSon.add(parent.get(i).getKnow().get(j).getServiceName());
					if(!includeStringInVertexes(vertexes, parent.get(i).getKnow().get(j).getServiceName()))
					// add to vertexes a vertex object (not duplicated) with the correct properties
					vertexes.add(addVertex(g, parent.get(i).getKnow().get(j).getServiceName()));
					vertexes.get(getPointer(vertexes,parent.get(i).getServiceName())).addEdge(
							//type of communication
							"call",
							//end of edge
							vertexes.get(getPointer(vertexes,parent.get(i).getKnow().get(j).getServiceName())),
							//weight property
							"weight", findWeight(spans, vertexes.get(getPointer(vertexes, parent.get(i).getServiceName())).value("name"), vertexes.get(getPointer(vertexes, parent.get(i).getKnow().get(j).getServiceName())).value("name")),
							"isDB", parent.get(i).getKnow().get(j).getHasDB()
					);
				}
				// to connect the services you need to use two vertexes pointers
				// a pointer to service_interface A and a pointer to service_interface B
				//int a = getPointer(vertexes, parent.get(i).getServiceName());
				//int b = getPointer(vertexes, parent.get(i).getKnow().get(j).getServiceName());
			}
			

			uniqueSon.clear();
			removing.clear();
		}
		
	
		OutputStream fop = null;
		File file;

		try {

			file = new File(path);
			fop = new FileOutputStream(file);
			GraphMLWriter.build().create().writeGraph(fop, g);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public int getPointer(List<Vertex> v, String name) {
		for(int i = 0; i < v.size(); i++) {
			if(v.get(i).value("name").toString().compareTo(name)==0)
				return i;
		}
		return -1;
	}
	/**
	 * @param g
	 */
	public Vertex addVertex(Graph g, String name) {
		Vertex v;
		v = g.addVertex("service_interface");
		v.property("name", name);
		return v;
	}
	private boolean includeStringInList(List<String> list, String name) {
		for(int i = 0; i < list.size(); i++) {
			if(name.compareTo(list.get(i)) == 0)
				return true;
		}
		return false;
		
	}
	private boolean includeStringInVertexes(List<Vertex> list, String name) {
		for(int i = 0; i < list.size(); i++) {
			if(name.compareTo(list.get(i).value("name")) == 0)
				return true;
		}
		return false;
		
	}
	
	private int findSpanbyServiceName(List<Span> parent, Span b) {
		int j = -1;
		for(int i = 0; i < parent.size(); i++) {
			if(compareSpanServiceName(parent.get(i), b))
					j = i;
		}
		return j;
	}
	
	private boolean compareSpanServiceName(Span a, Span b) {
		if(a.getServiceName().compareTo(b.getServiceName()) == 0)
			return true;
		return false;
	}
	
	
	/**
	 * @param list
	 * @param a
	 * @return
	 */
	private int findWeight(List<Span> list, String a, String b) {
		int weight = 0;
	    	for(int i = 0; i < list.size(); i++) {

	    		if(a.compareTo(list.get(i).getRemoteEndpoint().getServiceName()) == 0
	    				&&
	    			b.compareTo(list.get(i).getServiceName()) == 0)

	    			weight++;
	    	}

		return weight;

	}
	
	/**
	 * @param a
	 * @return
	 */
	private boolean isUniquee(String a) {
		if(singleOccurrence.size()==0)
			return true;
		for(String str: singleOccurrence) {
		    if(str.contains(a)) {
		       return false;}
		}
		return true;
	}
	
	public Graph getG() {
		return this.g;
	}
}
