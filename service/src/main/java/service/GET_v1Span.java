/*package service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.io.graphml.GraphMLWriter;
import org.apache.tinkerpop.gremlin.tinkergraph.structure.TinkerGraph;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import model.EndPoint;
import model.Span;
import service_interface.GraphBuilder;

@Service
public class GET_v1Span  {
	private List<Span> spans;
	private List<Span> parent;
	private List<String> singleOccurrence;
	private static final String filename = "spans.json";
	private Graph g = null;
	private final String path = "graph.graphml";
	
	/**
	 *  Constructor
	 * @return 
	 */
/*	public GET_v1Span() {
		this.singleOccurrence = new ArrayList<String>();
		this.spans = new ArrayList<Span>();
		this.parent = new ArrayList<Span>();
		this.g = TinkerGraph.open();
	}
    
	
	public void createV1() {
		populateList();
		generateCallGraph();
	}
	private void populateList() {
		//Span class parameters
		String id = "";
		String parentId = "";
		int duration = 1;
		String serviceNameL = "";
		String serviceNameR = "";
		Date date;
		String timeUTC = "";
		//EndPoint class parameters
		int localPort = -1;
		int remotePort = -1;
		String localIpv4 = "";
		String remoteIpv4 = "";
		
		Gson gson = new Gson();
		Object obj = null;
		
		try {
			obj = gson.fromJson(new FileReader(filename), Object.class);
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		JsonElement je = gson.toJsonTree(obj);
		JsonArray ja = je.getAsJsonArray();
		// populate span list from json
		for(int i = 0; i < ja.size();i++) {
			JsonArray ja_a = ja.get(i).getAsJsonArray();
			JsonObject ja_a_Obj = ja_a.get(0).getAsJsonObject();
			for(int j = 0; j < ja_a.size(); j++) {
				ja_a_Obj =  ja_a.get(j).getAsJsonObject();
				// String id cannot be null
				id = ja_a_Obj.get("id").toString();
				// String parentId can be null
				parentId = "";
				if(ja_a_Obj.has("parentId")) 
					parentId = ja_a_Obj.get("parentId").getAsString();
				// int duration min value = 1
				duration = ja_a_Obj.get("duration").getAsInt();
				// String timeUTC can be null
				timeUTC = "";
				if(ja_a_Obj.has("timestamp")) {
					long epochTime = TimeUnit.SECONDS.convert(ja_a_Obj.get("timestamp").getAsLong(), TimeUnit.MILLISECONDS);
					date = new Date(epochTime);
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH-mm z");
					formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
					timeUTC = formatter.format(date).toString();
				}
				serviceNameL = "";
				serviceNameR = "";
				//if localEndpoint and remoteEndpoint exists
				if(ja_a_Obj.has("localEndpoint") && ja_a_Obj.has("remoteEndpoint")) {
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("serviceName"))
						serviceNameL = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("serviceName").getAsString();
					
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("port"))
						localPort = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("port").getAsInt();
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("ipv4"))
						localIpv4 = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("ipv4").getAsString();
					
					if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("serviceName"))
						serviceNameR = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("serviceName").getAsString();
					if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("port"))
						remotePort = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("port").getAsInt();
					if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("ipv4"))
						remoteIpv4 = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("ipv4").getAsString();
					
					spans.add(new Span(
							id,
							parentId,
							duration,
							timeUTC,
							serviceNameL,
							new EndPoint(id,"local",localPort,localIpv4,serviceNameL),
							new EndPoint("","remote",remotePort,remoteIpv4, serviceNameR)					
							));
				}
				//if ONLY localEndpoint exists
				else if(ja_a_Obj.has("localEndpoint")){
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("serviceName"))
						serviceNameL = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("serviceName").getAsString();
					
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("port"))
						localPort = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("port").getAsInt();
					if(ja_a_Obj.get("localEndpoint").getAsJsonObject().has("ipv4"))
						localIpv4 = ja_a_Obj.get("localEndpoint").getAsJsonObject().get("ipv4").getAsString();
					
					remotePort = -1;
					remoteIpv4 = "";
					
					spans.add(new Span(
							id,
							parentId,
							duration,
							timeUTC,
							serviceNameL,
							new EndPoint(id,"local",localPort,localIpv4,serviceNameL),
							new EndPoint("","remote",remotePort,remoteIpv4,serviceNameR)						
							));
				} 
				//if ONLY remoteEndpoint exists
				else if(ja_a_Obj.has("remoteEndpoint")){
						if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("serviceName"))
							serviceNameR = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("serviceName").getAsString();
						if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("port"))
							remotePort = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("port").getAsInt();
						if(ja_a_Obj.get("remoteEndpoint").getAsJsonObject().has("ipv4"))
							remoteIpv4 = ja_a_Obj.get("remoteEndpoint").getAsJsonObject().get("ipv4").getAsString();	
				} 
			}	
		
		}
		String parentIdRelated = "";
		//populate i remoteEndPoint di ogni span con il parent associato
		for(int k = 0; k < spans.size(); k++) {
			//se esiste un parentId allora devo aggiungere il nome del servizio ai servizi associati nel remote
			if(spans.get(k).getParentId() != "")
				//ottenuto il parent id da usare per la ricerca dei remote da fillare
				parentIdRelated = spans.get(k).getParentId();
			for(int r = 0; r < spans.size(); r++) {
				if(spans.get(r).getParentId().compareTo(parentIdRelated) == 0 && parentIdRelated != "" && spans.get(r).getParentId() == "") {
					spans.get(r).getRemoteEndpoint().setServiceName(spans.get(k).getServiceName());
				}
			}
		}
	}
	
	
	/**
	 * @param listToPrint
	 */
	/*private void printList(List<Span> listToPrint) {
		System.out.println("startPrint");
		for(int i = 0; i < listToPrint.size(); i++)
			System.out.println("["+i+"]"+listToPrint.get(i).toString());
		System.out.println("endPrint");
	}

	/**
	 * @param listToPrint
	 */
/*	private void printListDebug(List<String> listToPrint) {
		System.out.println("startPrint");
		for(int i = 0; i < listToPrint.size(); i++)
			System.out.println("["+i+"]"+listToPrint.get(i).toString());
		System.out.println("endPrint");
	}
	private void printStructure() {
		printList(parent);
	}
	/**
	 * @param list
	 * @param name
	 * @return
	 */
/*	private boolean containsName(final List<Span> list, final String name){
	    return list.stream().filter(o -> o.getServiceName().equals(name)).findFirst().isPresent();
	}
	private void generateCallGraph() {
		String parentId;
		
		
		
		// populate parent list
		for(int i = 0; i < spans.size(); i++) {
			parentId = spans.get(i).getParentId();
			//if the span is a parent
			if(parentId == "") {
				//if it's unique and not present in parent list -> add into parent list
				if(!containsName(parent, spans.get(i).getServiceName().toString() )) {
					parent.add(spans.get(i));
				}
			}
		}
		printList(spans);
		String nametofind = "";
		String namefound = "";
		// populate know list for each parent
		for(int i = 0; i < parent.size(); i++) {
			//get the parent id 
			nametofind = parent.get(i).getServiceName();
			//looking for dependencies
			for(int j = 0; j < spans.size(); j++) {
				namefound = spans.get(j).getServiceName();
				//check the same row, based on id and serviceName
				if(nametofind.compareTo(namefound) == 0 && spans.get(j).getRemoteEndpoint().getServiceName() != "") {
					//parent.get(i).getKnow().add(spans.get(j).getRemoteEndpoint()));
					parent.get(i).getKnow().add(spans.get(j));
				}			
			}
		}
		printList(parent);

		List<Vertex> vertexes = new ArrayList <Vertex>();
		List<String> uniqueParent = new ArrayList <String> ();
		// look for the serviceName to which to link the list in position i of parent
		// do not create duplicates of the objects, I create a list of vertex addresses associated with the service_interface
		for(int i = 0; i < parent.size(); i++) {
			for(int j = 0; j < parent.get(i).getKnow().size(); j++) {
				// serviceName remoteEndpoint (B) in know not present in the uniqueParent list
				if(!includeStringInList(uniqueParent, parent.get(i).getKnow().get(j).getRemoteEndpoint().getServiceName())) {
					uniqueParent.add(parent.get(i).getKnow().get(j).getRemoteEndpoint().getServiceName());
					// I add to vertexes a vertex object (not duplicated) with the correct properties
					vertexes.add(addVertex(g,	parent.get(i).getKnow().get(j).getRemoteEndpoint().getServiceName(),
												parent.get(i).getKnow().get(j).getRemoteEndpoint().getPort()));
				}
				// serviceName of parent (A or B) not present in uniqueParent
				if(!includeStringInList(uniqueParent, parent.get(i).getLocalEndpoint().getServiceName())) {
					uniqueParent.add(parent.get(i).getLocalEndpoint().getServiceName());
					// I add to vertexes a vertex object (not duplicated) with the correct properties
					vertexes.add(addVertex(g,	parent.get(i).getKnow().get(j).getServiceName(),
												parent.get(i).getKnow().get(j).getLocalEndpoint().getPort()));
				}

				
				// to connect the services you need to use two vertexes pointers
				// a pointer to service_interface A and a pointer to service_interface B
				int a = getPointer(vertexes, parent.get(i).getServiceName());
				int b = getPointer(vertexes, parent.get(i).getKnow().get(j).getRemoteEndpoint().getServiceName());
				
			
				if(j==0) {
					vertexes.get(a).addEdge(
						//type of communication
						"call",
						//end of edge
						vertexes.get(b),
						//weight property
						"weight", findWeight(spans, vertexes.get(a).value("name"), vertexes.get(b).value("name"))
					);
				}else {
					boolean newvertex = true;
					for(int d = j-1; d >= 0; d--) {
						if(parent.get(i).getKnow().get(d).getRemoteEndpoint().getServiceName().compareTo(parent.get(i).getKnow().get(j).getRemoteEndpoint().getServiceName())==0)
							newvertex = false;
					}
					if(newvertex == true) {
						System.out.println("sto creando un nuovo edge");
						vertexes.get(a).addEdge(
								//type of communication
								"call",
								//end of edge
								vertexes.get(b),
								//weight property
								"weight", findWeight(spans, vertexes.get(a).value("name"), vertexes.get(b).value("name")));
						}
				}
			}
		}
		
	
		OutputStream fop = null;
		File file;

		try {

			file = new File(path);
			fop = new FileOutputStream(file);
			GraphMLWriter.build().create().writeGraph(fop, g);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private int getPointer(List<Vertex> v, String name) {
		for(int i = 0; i < v.size(); i++) {
			if(v.get(i).value("name").toString().compareTo(name)==0)
				return i;
		}
		return -1;
	}
	
	
	private Vertex addVertex(Graph g, String name, int port) {
		Vertex v;
		v = g.addVertex("service_interface");
		v.property("name", name);
		v.property("port", port);
		return v;
	}
	private boolean includeStringInList(List<String> list, String name) {
		for(int i = 0; i < list.size(); i++) {
			if(name.compareTo(list.get(i)) == 0)
				return true;
		}
		return false;
		
	}
	
	private int findSpanbyServiceName(List<Span> parent, Span b) {
		int j = -1;
		for(int i = 0; i < parent.size(); i++) {
			if(compareSpanServiceName(parent.get(i), b))
					j = i;
		}
		return j;
	}
	
	private boolean compareSpanServiceName(Span a, Span b) {
		if(a.getServiceName().compareTo(b.getServiceName()) == 0)
			return true;
		return false;
	}
	
	
	private int findWeight(List<Span> list, String a, String b) {
		int weight = 0;
	    	for(int j = 0; j < list.size(); j++) {

	    		if(a.compareTo(list.get(j).getServiceName()) == 0
	    				&&
	    			b.compareTo(list.get(j).getRemoteEndpoint().getServiceName()) == 0)

	    			weight++;
	    	}

		System.out.println("weight"+weight + " " + a);
		return weight;

	}
	
	private boolean isUniquee(String a) {
		if(singleOccurrence.size()==0)
			return true;
		for(String str: singleOccurrence) {
		    if(str.contains(a)) {
		       return false;}
		}
		return true;
	}
	
	public Graph getG() {
		return this.g;
	}

	

	
}
*/