# Aroma
### A tool for: Automatic Recovery of Microservices Architecture

The microservices architectural style gained popularity within the past few years, due to the many advantages it brings to the organizations that adopt it.
> "An application designed with microservices 
> consists of a group of loosely coupled services 
> which communicate with lightweight protocols"
> -Martin Fowler

### The aim
The past few years saw the rise of microservices studies and best practices, along with a wide industrial adoption of this architectural style. We now witness the birth of another challenging topic: microservices quality. Like other kinds of architectures, also microservices suffer from erosion, whose symptoms can be, for instance, the appearance of *microservices smells*, which impact negatively on the system's quality, by hindering, for example, its maintainability. We propose a tool, called AROMA, for the automatic reconstruction of microservices architectures and the detection  of microservices smells, based on the dynamic analysis of microservices execution traces. 
The tool is able to reverse engineer microservices applications and build the dependency graph 
of such architectures i.e. the high-level representation of microservices and their dependencies.
The tool relies mainly on Zipkin, a distributed tracing system able to collect dynamic data of microservices systems.
Zipkin offers a wide set of libraries to provide instrumentation on various platforms, allowing us to analyse different kinds of microservices implementations.
The only requirement to start the analysis on a project is to instrument it with Zipkin, prior Aroma execution.
At the end of the execution, Aroma produces a .graphml file containing the dependency graph.

### Supported microservices smells
* Lack of API Gateway
* Cyclic Dependency
* Shared Persistence

Learn more at: 
* Taibi et al: [Microservices Anti-Patterns: A Taxonomy]
* Pigazzini et al: [Towards Microservice Smells Detection]

### Tech

In order to build this tool, many technologies have been used. 
Below is a summary table of what has been applied:

* [Apache Maven] - v3.6.3
* [Zipkin] - v2.21
* [SwaggerUI] - v3.0
* [Gson] - v2.8.6
* [TinkerPop] - v3.4.8
* [Gremlin-Java] - v3.4.8

### Flow
![](https://i.ibb.co/WnW56FB/Flowchart.png)
### Data model
![](https://i.ibb.co/L9904Xh/datamodelgraph.png)
### Installation
The development of this tool was done on Ubuntu 20.04 with Apache Maven 3.6.3 and jdk 14.0.2.
##### Prerequisites
It is recommended Ubuntu 18.04 or further Ubuntu-based distribution.
To allow installation of all packages on Ubuntu, you must be logged in as user with sudo privileges.
```sh
$ sudo
```

##### Apache Maven + OpenJdk
Maven and Java are requested to run AROMA. Execute the following commands to install them.

The first command downloads the package lists from the repositories and updates them on
the newest versions of packages together with their dependencies:
```sh
$ sudo apt update
```
The second command intalls the OpenJDK package:
```sh
$ sudo apt install maven
```
To find out the installed version, run:
```sh
$ mvn -version
```
##### Clone the project
Clone locally the Aroma repository:
```sh
$ git clone https://gitlab.com/essere.lab/tools/aroma.git
```

##### Eclipse IDE
Install your favorite desktop IDE packages, Eclipse is recommended (another option could be IntelliJ IDEA):
```sh
$ sudo apt install default-jre
```
Download and install Eclipse snap package:
```sh
$ sudo snap install –classic eclipse
```

### Usage
Let's start with a brief description of the structure of the project and then explain how to set up a demo.

The project consists of 5 classes: 
* Controller.java 
* CallGraphV2Spans.java 
* Span.java 
* EndPoint.java 
* SmellsFinder.java

##### Controller
This is the Aroma entrypoint. 
Controller takes care of making the HTTP GET call to the zipkin collector, and stores in the specified *path* the json file containing the Zipkin traces.

##### CallGraphV2Spans
CallGraphV2Spans deals with reading the json file, populating the spans list and filtering it to obtain the parent list on which to make the connections for the creation of the dependency graph. It is working on the Zipkin's v2 json format.
##### Span
Span contains all the information associated with the service, inside it is also instantiated a **know** list which represents the list of Spans that are "known" by the current span.
##### EndPoint
The EndPoint class is used to save information about the localEndPoint and the remoteEndPoint. This class is instantiated by the Span class in two ways such as: remote and local using the type attribute inside the EndPoint class.
##### SmellsFinder
SmellsFinder deals with the search for smells microservices by analyzing the generated graph. Two automatic search methods have been implemented such as: 
* apiGateway() 
* cyclicDependency()
* sharedPersistence()

Respectively the first searches for the presence of a MegaService while the second searches for the lack of an API Gateway in the system.

##### Run AROMA
Once the project to analyze has been instrumented with Zipkin, directly or through OpenTelemetry API, it must be executed in order to collect the traces.
The traces are generated by the requests exchanged by the different services of the project under analysis: you can obtain them by interacting with the project UI (see the examples project below). Once you have collected a sufficient number of traces, it is possible to run Aroma. After its execution, in the Aroma output folder you will find two files:
* .json which contains all the spans obtained from the HTTP call.
* .graphml the file containing the generated graph.

From Aroma root folder
```sh
$ mvn clean install
```

Let's run it
```sh
$ cd /application
$ mvn spring-boot:run
```

### Example projects
You can run AROMA on your projects, **previous Zipkin instrumentation**. The list of Zipkin available instrumented libraries is [here]. 
If you want to run AROMA on projects which are already instrumented by Zipkin, you can try the following:

* [Spring Petclinic Microservices]
* [LAB Insurance Sales Portal]


_Spring-PetClinic-microservices_ is the distributed version of the Spring PetClinic sample application, developed to support the learning and usage of the Spring framework. Its API is instrumented through the use of openZipkin.
Thanks to the Zipkin traces, our tool can recover all the three backend services and also the API gateway. Moreover, it stores the number of requests received by each service. Notice that one of the nodes, customers-service, is dispatched with more requests than the other services, i.e., the edge which connects the gateway to this service has weight equals to 12. By checking the [documentation](https://github.com/spring-petclinic/spring-petclinic-microservices), this is justified by the fact that the service addresses both the management of the customer entity and the pet entity. Thanks to trace analysis, we are able to identify services requests and to detect the services which maximise this attribute. Concerning the detection of microservices smells, Aroma did not found instances in this project.

![](https://i.ibb.co/YygW67Z/grafo.png)

_LAB Insurance Sales Portal_ is a project developed by Alktom Lab, a simple insurance sales system designed with a microservice architecture using the [Micronaut framework](https://micronaut.io/). AROMA identifies six microservices, the API Gateway and also [_Consul_](https://www.consul.io/), a discovery service, used to enable the other services to discover each other by storing location information (like IP addresses) in a single registry. This is why all dependencies point to this service, because it acts as a proxy. Even if being able to detect the Consul service is useful in terms of completeness, it hides the actual interaction among the microservices. Also [Granchelli et. al](https://research.vu.nl/en/publications/towards-recovering-the-software-architecture-of-microservice-base) faced this problem while developing their tool: they implemented a solution where the software architect can indicate, after the reconstruction of the architecture, which services are actually discovery services, through a graphical editor. We aim in the future to automatise the detection of discovery services, to improve the generation of the call graph.

![](https://i.ibb.co/mXR0FTH/micronaut.png)

[Apache Maven]: <http://maven.apache.org/>
[Zipkin]: <https://zipkin.io/>
[SwaggerUI]: <https://swagger.io/tools/swagger-ui/>
[Gson]: <https://github.com/google/gson>
[TinkerPop]: <https://tinkerpop.apache.org/>
[Gremlin-Java]: <https://tinkerpop.apache.org/docs/current/tutorials/gremlin-language-variants/>
[Microservices Anti-Patterns: A Taxonomy]: <https://www.researchgate.net/publication/335135196_Microservices_Anti-Patterns_A_Taxonomy>
[Towards Microservice Smells Detection]: <https://www.researchgate.net/publication/346678327_Towards_Microservice_Smells_Detection>
[Spring Petclinic Microservices]:<https://github.com/spring-petclinic/spring-petclinic-microservices>
[LAB Insurance Sales Portal]:<https://github.com/asc-lab/micronaut-microservices-poc>
[here]: <https://zipkin.io/pages/tracers_instrumentation>







