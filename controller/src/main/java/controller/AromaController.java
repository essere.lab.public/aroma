package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import service_interface.GraphBuilder;
import service_interface.SmellsAnalyze;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

@Controller
public class AromaController {
	//HTTP properties
		private static final String USER_AGENT = "Mozilla/5.0";
		//private static final String GET_URL = "http://localhost:9411/api/v2/traces?limit=500&lookback=400000";
		private static final String GET_URL = "http://localhost:9411/api/v2/traces?limit=500&lookback=400000";
		// Directory path
		public static String path = "";
	@Autowired
	GraphBuilder gb;
	/*
	@Autowired
	HttpQuery hq;*/
	@Autowired
	SmellsAnalyze sa;

	public AromaController(GraphBuilder gb, SmellsAnalyze sa/*, HttpQuery hq, SmellsAnalyze sc*/) {
		this.gb = gb;
		this.sa = sa;
		/*this.hq = hq;
		this.sc = sc;*/
	}

	public void aromaController() {
		createAbsolutePath();
		gb.createV2();
		//GET_v2Span graph = new GET_v2Span();
		//CallGraphV1Spans graph = new CallGraphV1Spans();
		//graph.create();
		//graph.printStructure();
		System.out.println("\n"+"SmellsFinder"+"\n");
		sa.lackAPIGateway();
		sa.cyclicDependency();
		sa.sharedPersistence();

	}

	private static void createAbsolutePath() {
		try {

	        String filename = "spans.json";
	        String workingDirectory = System.getProperty("user.dir");
	            
	        //absoluteFilePath = workingDirectory + System.getProperty("file.separator") + filename;
	        path = workingDirectory + File.separator + filename;

	        System.out.println("Final filepath : " + path);
	            
	        File file = new File(path);
			if(file.exists() && !file.isDirectory()) {
				System.out.println("File exists");
			}else {
				file.createNewFile();
				getStream();
			}

	      }catch (IOException e) {
	        e.printStackTrace();
	      }
	}

	private static void getStream()  {
		try {
		URL obj = new URL(GET_URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = con.getResponseCode();
		// responseCode == 200
		if (responseCode == HttpURLConnection.HTTP_OK) { 
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			// Append to String
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			// Save into file
			fileWriter(response);
		} else {
			System.out.println("GET request not worked");
		}}catch(Exception ignored) {
		}
	}

	private static void fileWriter(StringBuffer sbf) throws IOException {
		// Build gson object to parse StringBuffer into String
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(sbf.toString());
		// Parse JsonElement to String
		String prettyJsonString = gson.toJson(je);
		// Open the stream
		BufferedWriter bwr = new BufferedWriter(new FileWriter(path));
		// Write contents of StringBuffer to a file
		bwr.write(prettyJsonString);
		// Flush the stream
		bwr.flush();
		// Close the stream
		bwr.close();
	}
	
}
