package application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;

import controller.AromaController;
import service_interface.GraphBuilder;
import service_interface.HttpQuery;
import service_interface.SmellsAnalyze;

@SpringBootApplication(scanBasePackages= {"domain", "application", "service"})
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class Runner {
	@Autowired
	GraphBuilder gb;
	/*@Autowired
	HttpQuery hq;*/
	@Autowired
	SmellsAnalyze sa;
	public static void main(String[] args) {
    	ApplicationContext context = SpringApplication.run(Runner.class, args);
    	AromaController controller = new AromaController(context.getBean(GraphBuilder.class)/*,context.getBean(hq.class)*/,context.getBean(SmellsAnalyze.class));
    	controller.aromaController();
    	System.exit(0);
    }
}
