package service_interface;

import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public interface SmellsAnalyze {
    void lackAPIGateway();
    void megaService();
    void cyclicDependency();
    void sharedPersistence();
    boolean checkCycle(int node,  ArrayList<ArrayList<Integer>> adj, int vis[], int dfsVis[]);
    boolean isCyclic(int N, ArrayList<ArrayList<Integer>> adj);
}
