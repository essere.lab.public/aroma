package service_interface;


import org.springframework.stereotype.Component;


@Component
public interface GraphBuilder {
	boolean createV2();
}
