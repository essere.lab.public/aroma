package model;

public class EndPoint {
	private String id;
	private String type;
	private int port;
	private String ipv4;
	private String serviceName;
	/**
	 * Constructor
	 * @param id
	 * @param type
	 * @param port
	 * @param ipv4
	 * @param serviceName
	 */
	public EndPoint(String id, String type, int port, String ipv4, String serviceName) {
		this.port = port;
		this.type = type;
		this.ipv4 = ipv4;
		this.id = id;
		this.serviceName = serviceName;
	}
	public int getPort() {
		return port;
	}
	
	public String getType() {
		return type;
	}
	public String getId() {
		return id;
	}
	public String getIpv4() {
		return ipv4;
	}
	
	public String getServiceName() {
		return serviceName;
	}
	
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	@Override
	public String toString() {
		
		return type+"EndPoint:"+" [id=" + id + ", port=" + port + ", ipv4=" + ipv4 + ", serviceName=" + serviceName +"]";
	}
}
