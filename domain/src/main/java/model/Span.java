package model;

import java.util.ArrayList;
import java.util.List;

import org.apache.tinkerpop.gremlin.structure.Vertex;

public class Span {
	private String id;
	private String parentId;
	private String serviceName;
	private String timeUTC;
	private String ipv4;
	private int duration;
	private EndPoint local;
	private EndPoint remote;
	private Vertex v;
	//private List<EndPoint> know;
	private List<Span> know;
	private Boolean hasDB;
	private String dbName;


	/**
	 * Constructor
	 * @param id
	 * @param parentId
	 * @param duration
	 * @param timeUTC
	 * @param serviceName
	 * @param local
	 * @param remote
	 */
	public Span(String id, String parentId,  int duration, String timeUTC, String serviceName, EndPoint local, EndPoint remote, String dbName, Boolean hasDB) {
		super();
		this.id = id;
		this.parentId = parentId;
		this.serviceName = serviceName;
		this.duration = duration;
		this.timeUTC = timeUTC;
		this.local = local;
		this.remote = remote;
		//this.know = new ArrayList<EndPoint>();
		this.know = new ArrayList <Span>();
		this.dbName = dbName;
		this.hasDB = hasDB;
	}
	
	
	public String getId() {
		return id;
	}
	public String getParentId() {
		return parentId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public int getDuration() {
		return duration;
	}
	public String getTimeUTC() {
		return timeUTC;
	}
	public String getIpv4() {
		return ipv4;
	}
	public EndPoint getLocalEndpoint() {
		return local;
	}
	public EndPoint getRemoteEndpoint() {
		return remote;
	}
	public Vertex getV() {
		return v;
	}
	public String getDbName() { return dbName; }
	public Boolean getHasDB() { return hasDB; }
	/*
	public List<EndPoint> getKnow(){
		return know;
	}*/
	
	public List<Span> getKnow(){
		return know;
	}
	@Override
	public String toString() {
		String a = "Span [id=" + id + ", parentId=" + parentId + ", serviceName=" + serviceName + ", dbName=" + dbName + ", hasDB=" + hasDB;
		String b = ", " + local.toString() + ", " +remote.toString();
		String c = "";
		for(int i = 0; i < know.size(); i++)
			c = c + ", " + i +":" + know.get(i).getRemoteEndpoint().getServiceName();
		c = c + " ]";
		return a+b+c;
	}
	public String toStringV2() {
		String a = "Span [id=" + id + ", parentId=" + parentId + ", serviceName=" + serviceName + ", dbName=" + dbName + ", hasDB=" + hasDB;
		String b = ", " + local.toString() + ", " +remote.toString();
		String c = "";
		for(int i = 0; i < know.size(); i++)
			c = c + ", " + i +":" + know.get(i).getServiceName();
		c = c + " ]";
		return a+b+c;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((serviceName == null) ? 0 : serviceName.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Span other = (Span) obj;
		if (serviceName == null) {
			if (other.serviceName != null)
				return false;
		} else if (!serviceName.equals(other.serviceName))
			return false;
		
		return true;
	}

	
}
